/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package git.tutorial;

/**
 *
 * @author Pedro Pereira
 */
public class Duck {

    public Duck() {

    }

    void makeSound(int loudness) {
        System.out.println("qua qua qua");
    }

    void layEgg() {
    }

    void quack() {
    }

    void blink() {
    }

    void shake() {
    }

    void swim() {
    }

    void eat() {
    }
    
    void sleep() {}
    
}
